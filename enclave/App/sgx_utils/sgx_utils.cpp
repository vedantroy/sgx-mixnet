#include <iostream>
#include "sgx_urts.h"
#include "sgx_utils.h"

/*
Description: Creates an enclave, which is stored in the file "enclave.signed.so"
            Ignores the launch token, which is created by sgx_create_enclave()
            Normally the launch token should be saved to a file and re-used in future calls to sgx_create_enclave()
*/
//Reference reading: https://software.intel.com/en-us/sgx-sdk-dev-reference-sgx-create-enclave
void print_sgx_error(sgx_status_t status)
{
    std::cout << "SGX Error Code: " << status << std::endl;
}

int initialize_global_enclave(sgx_enclave_id_t *global_enclave_id)
{
    //sgx_create_enclave() parameters
    sgx_launch_token_t token = {0};
    int updated = 0;

    sgx_status_t result = sgx_create_enclave("enclave.signed.so", SGX_DEBUG_FLAG, &token, &updated, global_enclave_id, nullptr);

    if (result != SGX_SUCCESS)
    {
        print_sgx_error(result);
        return -1;
    }
    return 0;
}
