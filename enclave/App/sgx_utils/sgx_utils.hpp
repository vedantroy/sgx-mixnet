#include "sgx_urts.h"

#ifndef SGX_UTILS_H
#define SGX_UTILS_H

int initialize_global_enclave(sgx_enclave_id_t *global_enclave_id);
void print_sgx_error(sgx_status_t status);
#endif