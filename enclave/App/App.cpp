#include "sgx_utils/sgx_utils.hpp"
#include <iostream>
#include "Enclave_u.h"

sgx_enclave_id_t global_enclave_id = 0;

/*
void ocall_print(const char *str)
{
    std::cout << str << std::endl;
}
*/

int main(int argc, char const *argv[])
{
    if (initialize_global_enclave(&global_enclave_id) != 0)
    {
        std::cout << "Failed to initialize enclave" << std::endl;
        return 1;
    }

    int ret_val = 0;
    sgx_status_t status = ecall_create_server(global_enclave_id, &ret_val, argv[1]);
    if (status != SGX_SUCCESS)
    {
        std::cout << "ecall_create_server() failed" << std::endl;
        std::cout << status << std::endl;
        return 1;
    }
    return 0;
}