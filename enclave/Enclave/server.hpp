#pragma once
#ifndef SERVER_HPP
#define SERVER_HPP

#include <string>
#include <vector>

int ssl_server(const char *current_port);

#endif