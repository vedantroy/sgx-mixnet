#pragma once

#ifndef SERVER_INFO_HPP
#define SERVER_INFO_HPP

#include <string>

std::string ports[] = {"6000", "6001", "6002"};
int num_ports = 3;

#endif