#include "mbedtls/config.h"
#include "mbedtls/platform.h"

#include "mbedtls/net.h"
#include "mbedtls/ssl.h"
#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"
#include "mbedtls/certs.h"
#include "mbedtls/x509.h"
#include "mbedtls/error.h"
#include "mbedtls/debug.h"

//Personal includes
#include "client.hpp"
#include "server_info.hpp"

#include <stdlib.h>
#include <string>
#include <sgx_trts.h>

#define RELEASE
#ifdef RELEASE
#define mbedtls_printf
#endif

static void my_debug(void *ctx, int level,
                     const char *file, int line,
                     const char *str)
{
    const char *p, *basename;
    (void)ctx;

    /* Extract basename from file */
    for (p = basename = file; *p != '\0'; p++)
        if (*p == '/' || *p == '\\')
            basename = p + 1;

    mbedtls_printf("%s:%04d: |%d| %s", basename, line, level, str);
}

void print_last_error(int error_code)
{
    if (error_code != 0)
    {
        char error_buf[100];
        mbedtls_strerror(error_code, error_buf, 100);
        mbedtls_printf("Last error was: %d - %s\n\n", error_code, error_buf);
    }
}

int ssl_server(const char *current_port)
{
    int ret, len;
    mbedtls_net_context listen_fd, client_fd;
    unsigned char buf[1024];
    const char *pers = "ssl_server";

    mbedtls_entropy_context entropy;
    mbedtls_ctr_drbg_context ctr_drbg;
    mbedtls_ssl_context ssl;
    mbedtls_ssl_config conf;
    mbedtls_x509_crt srvcert;
    mbedtls_pk_context pkey;

    mbedtls_net_init_ocall(&listen_fd);
    mbedtls_net_init_ocall(&client_fd);
    mbedtls_ssl_init(&ssl);
    mbedtls_ssl_config_init(&conf);
    mbedtls_x509_crt_init(&srvcert);
    mbedtls_pk_init(&pkey);
    mbedtls_entropy_init(&entropy);
    mbedtls_ctr_drbg_init(&ctr_drbg);

#if defined(MBEDTLS_DEBUG_C)
    int DEBUG_LEVEL = 0;
    mbedtls_debug_set_threshold(DEBUG_LEVEL);
#endif

    /*
     * 1. Load the certificates and private RSA key
     */
    /*
     * This demonstration program uses embedded test certificates.
     * Instead, you may want to use mbedtls_x509_crt_parse_file() to read the
     * server and CA certificates, as well as mbedtls_pk_parse_keyfile().
     */
    /*
    ret = mbedtls_x509_crt_parse(&srvcert, (const unsigned char *)mbedtls_test_srv_crt,
                                 mbedtls_test_srv_crt_len);
    */
    ret = mbedtls_x509_crt_parse(&srvcert, (const unsigned char *)mbedtls_test_srv_crt,
                                 mbedtls_test_srv_crt_len);
    if (ret != 0)
    {
        mbedtls_printf(" failed\n  !  mbedtls_x509_crt_parse returned %d\n\n", ret);
        goto exit;
    }

    ret = mbedtls_x509_crt_parse(&srvcert, (const unsigned char *)mbedtls_test_cas_pem,
                                 mbedtls_test_cas_pem_len);
    if (ret != 0)
    {
        mbedtls_printf(" failed\n  !  mbedtls_x509_crt_parse returned %d\n\n", ret);
        goto exit;
    }

    ret = mbedtls_pk_parse_key(&pkey, (const unsigned char *)mbedtls_test_srv_key,
                               mbedtls_test_srv_key_len, NULL, 0);
    if (ret != 0)
    {
        mbedtls_printf(" failed\n  !  mbedtls_pk_parse_key returned %d\n\n", ret);
        goto exit;
    }

    mbedtls_printf(" ok\n");

    /*
     * 2. Setup the listening TCP socket
     */
    mbedtls_printf("  . Binding on port ");

    if ((ret = mbedtls_net_bind_ocall(&listen_fd, NULL, current_port, MBEDTLS_NET_PROTO_TCP)) != 0)
    {
        mbedtls_printf(" failed\n  ! mbedtls_net_bind returned %d\n\n", ret);
        goto exit;
    }

    mbedtls_printf(" setup listening TCP socket ok\n");

    /*
     * 3. Seed the RNG
     */
    mbedtls_printf("  . Seeding the random number generator...");

    if ((ret = mbedtls_ctr_drbg_seed(&ctr_drbg, mbedtls_entropy_func, &entropy,
                                     (const unsigned char *)pers,
                                     strlen(pers))) != 0)
    {
        mbedtls_printf(" failed\n  ! mbedtls_ctr_drbg_seed returned %d\n", ret);
        goto exit;
    }

    mbedtls_printf(" ok\n");

    /*
     * 4. Misc. setup
     */

    mbedtls_printf("  . Setting up the SSL data....");

    if ((ret = mbedtls_ssl_config_defaults(&conf,
                                           MBEDTLS_SSL_IS_SERVER,
                                           MBEDTLS_SSL_TRANSPORT_STREAM,
                                           MBEDTLS_SSL_PRESET_DEFAULT)) != 0)
    {
        mbedtls_printf(" failed\n  ! mbedtls_ssl_config_defaults returned %d\n\n", ret);
        goto exit;
    }

    mbedtls_ssl_conf_rng(&conf, mbedtls_ctr_drbg_random, &ctr_drbg);
    mbedtls_ssl_conf_dbg(&conf, my_debug, NULL);

    mbedtls_ssl_conf_ca_chain(&conf, srvcert.next, NULL);
    if ((ret = mbedtls_ssl_conf_own_cert(&conf, &srvcert, &pkey)) != 0)
    {
        mbedtls_printf(" failed\n  ! mbedtls_ssl_conf_own_cert returned %d\n\n", ret);
        goto exit;
    }

    if ((ret = mbedtls_ssl_setup(&ssl, &conf)) != 0)
    {
        mbedtls_printf(" failed\n  ! mbedtls_ssl_setup returned %d\n\n", ret);
        goto exit;
    }

    mbedtls_printf(" ok\n");

    while (true)
    {
        print_last_error(ret);

        mbedtls_net_free_ocall(&client_fd);

        mbedtls_ssl_session_reset(&ssl);

        /*
     * 3. Wait until a client connects
     */
        mbedtls_printf("  . Waiting for a remote connection ...");

        if ((ret = mbedtls_net_accept_ocall(&listen_fd, &client_fd,
                                            NULL, 0, NULL)) != 0)
        {
            mbedtls_printf(" failed\n  ! mbedtls_net_accept returned %d\n\n", ret);
            goto exit;
        }

        mbedtls_ssl_set_bio(&ssl, &client_fd, mbedtls_net_send_ocall, mbedtls_net_recv_ocall, NULL);

        mbedtls_printf(" ok\n");

        /*
     * 5. Handshake
     */
        mbedtls_printf("  . Performing the SSL/TLS handshake...");

        while ((ret = mbedtls_ssl_handshake(&ssl)) != 0)
        {
            if (ret != MBEDTLS_ERR_SSL_WANT_READ && ret != MBEDTLS_ERR_SSL_WANT_WRITE)
            {
                mbedtls_printf(" failed\n  ! mbedtls_ssl_handshake returned %d\n\n", ret);
                continue;
            }
        }

        mbedtls_printf(" ok\n");

        /*
     * 6. Read the HTTP Request
     */
        mbedtls_printf("  < Read from client:");

        while (true)
        {
            len = sizeof(buf) - 1;
            memset(buf, 0, sizeof(buf));
            ret = mbedtls_ssl_read(&ssl, buf, len);

            if (ret == MBEDTLS_ERR_SSL_WANT_READ || ret == MBEDTLS_ERR_SSL_WANT_WRITE)
                continue;

            if (ret <= 0)
            {
                switch (ret)
                {
                case MBEDTLS_ERR_SSL_PEER_CLOSE_NOTIFY:
                    mbedtls_printf(" connection was closed gracefully\n");
                    break;

                case MBEDTLS_ERR_NET_CONN_RESET:
                    mbedtls_printf(" connection was reset by peer\n");
                    break;

                default:
                    mbedtls_printf(" mbedtls_ssl_read returned -0x%x\n", -ret);
                    break;
                }
            }
            else
            {
                len = ret;
                mbedtls_printf(" %d bytes read\n\n%s", len, (char *)buf);
            }
            break;
        }

        int passCounter = buf[0] - '0';

        if (passCounter < 0 || passCounter > 9)
        {
            mbedtls_printf("Message is malformed. Stopping routing");
        }
        else if (passCounter == 9)
        {
            mbedtls_printf("Message has been shuffled 9 times. Stopping routing.");
        }
        else
        {
            buf[0]++;
            std::string current_port_string = current_port;
            std::string destination_port;
            do
            {
                uint32_t rand_num;
                sgx_read_rand((unsigned char *)&rand_num, 4);
                destination_port = ports[rand_num % num_ports];
            } while (current_port_string.compare(destination_port) == 0);
            //Unclear if (char *)buf is safe
            send_message("localhost", destination_port.c_str(), (char *)buf);
        }

        mbedtls_printf("  . Closing the connection...");

        while ((ret = mbedtls_ssl_close_notify(&ssl)) < 0)
        {
            if (ret != MBEDTLS_ERR_SSL_WANT_READ &&
                ret != MBEDTLS_ERR_SSL_WANT_WRITE)
            {
                mbedtls_printf(" failed\n  ! mbedtls_ssl_close_notify returned %d\n\n", ret);
                continue;
            }
        }

        mbedtls_printf(" closed connection\n");

        ret = 0;
    }

exit:
    mbedtls_printf("Exiting...");

    print_last_error(ret);

    mbedtls_net_free_ocall(&client_fd);
    mbedtls_net_free_ocall(&listen_fd);

    mbedtls_x509_crt_free(&srvcert);
    mbedtls_pk_free(&pkey);
    mbedtls_ssl_free(&ssl);
    mbedtls_ssl_config_free(&conf);

    mbedtls_ctr_drbg_free(&ctr_drbg);
    mbedtls_entropy_free(&entropy);

    return (ret);
}

#undef MBEDTLS_CONFIG_FILE