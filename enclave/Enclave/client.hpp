#pragma once
#ifndef CLIENT_H
#define CLIENT_H

int send_message(const char *server_name, const char *server_port, const char *message);

#endif