#ifndef SERVERLIST_H
#define SERVERLIST_H

#include <vector>
#include <string>

const std::string ports[4] = {"9600", "9601", "9602", "9603"};
const char num_ports = 4;
#endif
