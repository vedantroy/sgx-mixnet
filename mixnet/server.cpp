#include <errno.h>
#include <unistd.h>
#include <malloc.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <resolv.h>
#include "openssl/ssl.h"
#include "openssl/err.h"
#include <iostream>
#include <cstring>

#include "SSLNetworkUtils.hpp"
#include "ServerList.hpp"

int isRoot()
{
    if (getuid() != 0)
    {
        return false;
    }
    else
    {
        return true;
    }
}

SSL_CTX *InitSSLServer(void)
{
    const SSL_METHOD *method = TLS_server_method();
    SSL_CTX *ctx = SSL_CTX_new(method);

    if (ctx == nullptr)
    {
        ERR_print_errors_fp(stderr);
        exit(EXIT_FAILURE);
    }
    return ctx;
}

void LoadCertificates(SSL_CTX *ctx, const char *CertFile, const char *KeyFile)
{
    /* set the local certificate from CertFile */
    if (SSL_CTX_use_certificate_file(ctx, CertFile, SSL_FILETYPE_PEM) <= 0)
    {
        ERR_print_errors_fp(stderr);
        abort();
    }
    /* private key Keyfile may be same as CertFile */
    if (SSL_CTX_use_PrivateKey_file(ctx, KeyFile, SSL_FILETYPE_PEM) <= 0)
    {
        ERR_print_errors_fp(stderr);
        abort();
    }
    if (!SSL_CTX_check_private_key(ctx))
    {
        fprintf(stderr, "Private key does not match the public certificate\n");
        abort();
    }
}

int main(int argc, char const *argv[])
{

    if (!isRoot())
    {
        std::cout << "Must run program as root" << std::endl;
        exit(EXIT_FAILURE);
    }

    SSL_CTX *ctx = InitSSLServer();
    SSL *ssl = SSL_new(ctx);
    if (ssl == nullptr)
    {
        fprintf(stderr, "SSL_new() failed\n");
        exit(EXIT_FAILURE);
    }

    LoadCertificates(ctx, "mycert.pem", "mycert.pem");
    int sfd = OpenConnection("127.0.0.1", argv[1], true);
    if (listen(sfd, 25) != 0)
    {
        perror("Can't configure listening port");
        exit(EXIT_FAILURE);
    }

    while (true)
    {
        sockaddr_in incoming_addr;
        socklen_t addr_size = sizeof incoming_addr;
        int incoming_fd = accept(sfd, (struct sockaddr *)&incoming_addr, &addr_size);
        //std::cout << "Incoming Connection: " << inet_ntoa(incoming_addr.sin_addr) << ":" << ntohs(incoming_addr.sin_port) << std::endl;
        //printf("Incoming Connection: %s:%d\n", inet_ntoa(incoming_addr.sin_addr), ntohs(incoming_addr.sin_port));
        SSL *ssl = SSL_new(ctx);
        SSL_set_fd(ssl, incoming_fd);

        char buf[1024] = {0};

        if (SSL_accept(ssl) == -1)
        {
            ERR_print_errors_fp(stderr);
        }
        else
        {
            //ShowCerts(ssl);
            int bytes = SSL_read(ssl, buf, sizeof(buf));
            buf[bytes] = '\0';
            if (bytes <= 0)
            {
                ERR_print_errors_fp(stderr);
            }
            else
            {
                std::cout << "Incoming message: " << buf << std::endl;
            }
        }
        int used_fd = SSL_get_fd(ssl);
        SSL_free(ssl);
        close(used_fd);

        int passCounter = buf[0] - '0';

        if (passCounter < 0 || passCounter > 9)
        {
            std::cout << "Message is malformed. Stopping routing" << std::endl;
        }
        else if (passCounter == 9)
        {
            std::cout << "Message has been shuffled 9 times. Stopping routing." << std::endl;
        }
        else
        {
            buf[0]++;
            std::string port = argv[1];
            do
            {
                port = ports[rand() % num_ports];
            } while (port.compare(argv[1]) == 0);
            std::cout << "Sending message to port: " << port << std::endl;
            sendMessage("127.0.0.1", port.c_str(), buf, 1024);
        }
    }
    SSL_CTX_free(ctx);
    close(sfd);
}