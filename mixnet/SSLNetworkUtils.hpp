#ifndef SSLNETWORKUTILS_H
#define SSLNETWORKUTILS_H

int OpenConnection(const char *hostname, const char *port, const bool isServer);
int sendMessage(const char *address, const char *port, const void *buf, int buf_len);
#endif
